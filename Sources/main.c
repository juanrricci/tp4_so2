/* ###################################################################
**     Filename    : main.c
**     Project     : TP4_SO2_nosdk
**     Processor   : MK64FN1M0VLL12
**     Version     : Driver 01.01
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-09-17, 12:48, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.01
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/         
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "Events.h"
#include "Pins1.h"
#include "FRTOS1.h"
#include "UTIL1.h"
#include "MCUC1.h"
#include "PTRC1.h"
#include "AS1.h"
#include "ASerialLdd1.h"
#include "ExtIntSW2.h"
#include "ExtIntLdd1.h"
/* Including shared modules, which are used for whole project */
#include "PE_Types.h"
#include "PE_Error.h"
#include "PE_Const.h"
#include "IO_Map.h"
#include "PDD_Includes.h"
#include "Init_Config.h"
/* User includes (#include below this line is not maintained by Processor Expert) */

xQueueHandle cola_datos; // declaración de la cola de datos

static void periodic_task( void * param ) // productor (escritor)
{
	/*
	 * falta chequeo de espacio en la cola
	 */
	( void )param;
	portBASE_TYPE xStatus;
	char caracter = 65;
	char i = 0;
	char caracterNuevo;

	for( ; ; )
	{
		caracterNuevo = caracter + i;
		xStatus = xQueueSendToBack( cola_datos, &caracterNuevo, 0 ); // se pone al final de la cola de datos el nuevo caracter generado
		vTaskDelay( 5 / portTICK_RATE_MS ); /* 5 ms de bloqueo hace que se desordene el buffer de UART */
		i++;

		if( i == 26 )
		{
			i = 0;
			caracterNuevo = 10; // nueva linea
			xStatus = xQueueSendToBack( cola_datos, &caracterNuevo, 0 ); // se pone al final de la cola de datos el nuevo caracter generado
			vTaskDelay( 5 / portTICK_RATE_MS ); /* 5 ms de bloqueo hace que se desordene el buffer de UART */
			caracterNuevo = 13; // retorno de carro
			xStatus = xQueueSendToBack( cola_datos, &caracterNuevo, 0 ); // se pone al final de la cola de datos el nuevo caracter generado
			vTaskDelay( 5 / portTICK_RATE_MS ); /* 5 ms de bloqueo hace que se desordene el buffer de UART */
		}
	} /* for */
}

static void random_task( void * param ) // productor (escritor): solo se deberia ejecutar triggereado por interrupcion externa
{
	/*
	 * falta chequeo de espacio en la cola
	 * falta garantizar la periodicidad de los datos generados
	 * (si hay demasiados bloqueos por parte de txUART, ¿como garantizamos que los datos sean periodicos?)
	 */
	( void )param;
	portBASE_TYPE xStatus;
	char u = 117;
	char LF = 10;
	char CR = 13;

	/*
	 * Cada vez que ingrese una interrupcion externa por SW2, se pondrán en la cabeza de la cola
	 * los caracteres en el orden correcto para generar un salto de linea y escribir una "u"
	 * en representación a que es una interrupcion generada por el usuario.
	 */
	for( ; ; )
	{
		xStatus = FRTOS1_xQueueSendToFront( cola_datos, &CR, 0 ); // 5°: retorno de carro
		xStatus = FRTOS1_xQueueSendToFront( cola_datos, &LF, 0 ); // 4°: nueva linea
		xStatus = FRTOS1_xQueueSendToFront( cola_datos, &u,  0 ); // 3°: caracter "u"
		xStatus = FRTOS1_xQueueSendToFront( cola_datos, &CR, 0 ); // 2°: retorno de carro
		xStatus = FRTOS1_xQueueSendToFront( cola_datos, &LF, 0 ); // 1°: nueva linea
		taskYIELD(); // inmediatamente termina la ejecucion anterior, se bloquea el task para permitir que el task periodico se ejecute
	} /* for */
}

static void txuart_task( void * param ) // consumidor (lector): solo se deberia ejecutar cuando haya interrupcion de buffer txUART disponible
{
	( void )param;
	portBASE_TYPE xStatus;
	char caracterAEnviar;

	for( ; ; )
	{
		if( FRTOS1_uxQueueMessagesWaiting( cola_datos )) // chequea que haya al menos 1 dato valido en la cola
		{
			xStatus = xQueueReceive( cola_datos, &caracterAEnviar, 0 ); // extrae el primer dato de la cola
			AS1_SendChar( caracterAEnviar ); // envia el dato por UART
			vTaskDelay( 20 / portTICK_RATE_MS ); // inmediatamente termina la ejecucion anterior, se bloquea el task para permitir que el task periodico se ejecute
		}
	} /* for */
}

/*lint -save  -e970 Disable MISRA rule (6.3) checking. */
int main( void )
/*lint -restore Enable MISRA rule (6.3) checking. */
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  PE_low_level_init();
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */

  cola_datos = FRTOS1_xQueueCreate( 1000, 			 // la cola almacena hasta 1000 elementos
		  	  	  	  	  	  	    sizeof( char )); // el tamaño de cada elemento es de 1 byte

  if( cola_datos != NULL ) // si la cola pudo ser creada, entonces se generan los tasks y se inicia el scheduler
  {
	  FRTOS1_xTaskCreate( periodic_task,
		  	  	 	 	  "periodic",
						  configMINIMAL_STACK_SIZE,
						  ( void * )NULL,
						  tskIDLE_PRIORITY,
						  NULL );

	  /*
	  FRTOS1_xTaskCreate( random_task,
		  	  	 	 	  "random",
						  configMINIMAL_STACK_SIZE,
						  ( void * )NULL,
						  1, // prioridad 1 (intermedia)
						  NULL);
	*/

	  FRTOS1_xTaskCreate( txuart_task,
		  	  	 	 	  "txuart",
						  configMINIMAL_STACK_SIZE,
						  ( void* )NULL, // se pasa el semaforo como parametro
						  2, // prioridad 2 (máxima)
						  NULL);

	  FRTOS1_vTaskStartScheduler();
  }

  for( ; ; );

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;){}
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.5 [05.21]
**     for the Freescale Kinetis series of microcontrollers.
**
** ###################################################################
*/
